import requests
import json

url = "http://128.199.24.51:6367/partsone"

payload = json.dumps({
  "description": "DIRECT CURRENT POWER SUPPLYManufacturer: Hillphoenix Cases SKU: P086845H OUT OF STOCK LEAD TIME - 2 WEEKS DCP,LED,24V,100W,W/CONN",
  "application_type": "power_supply"
})
headers = {
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
